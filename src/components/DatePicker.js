import React, { useState, useEffect } from "react";
import { View, Platform, StyleSheet } from "react-native";

import DateTimePicker from "@react-native-community/datetimepicker";
import { Button } from "react-native-elements";

export const DatePicker = ({
  startDateDefault,
  data,
  onCallbackFunction,
  updateDate,
  msg,
}) => {
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  useEffect(() => {
    if (data) {
      setDate(new Date(data));
    }
  }, []);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);

    onCallbackFunction(selectedDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  let dateStart = new Date();
  let dateEnd = new Date();
  dateEnd.setMonth(dateEnd.getMonth() + 5);

  if (!startDateDefault) dateStart.setDate(dateStart.getDate() - 10);

  if (updateDate) {
    if (new Date(updateDate) < dateStart) {
      dateStart = new Date(updateDate);
    }
  }

  const title = () => {
    if (data) {
      const d = new Date(data);
      return `${d.getDate()}.${d.getMonth()}.${d.getFullYear()}`;
    }
    return "-- -- ----";
  };

  return (
    <View style={styles.container}>
      <View>
        <Button
          onPress={showDatepicker}
          title={title()}
          type="outline"
          titleStyle={{
            color: "#000",
          }}
          onLongPress={() => {
            alert(msg);
          }}
        />
      </View>
      {show && (
        <DateTimePicker
          locale="uk"
          testID="dateTimePicker"
          value={date}
          mode={mode}
          display="default"
          onChange={onChange}
          minimumDate={dateStart}
          maximumDate={dateEnd}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 5,
    flexGrow: 1,
  },
});
