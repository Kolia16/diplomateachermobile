import React, { useState } from "react";
import { StyleSheet, View, Picker, Dimensions } from "react-native";
import { useSelector } from "react-redux";

import { Button } from "react-native-elements";
import { AppText } from "./ui";
import { dataCourses } from "../data";

import { THEME } from "../theme";

const getCourse = (course)=>{
  let dataCourse = dataCourses.find(x=>course == x.number)
  return dataCourse
}

export const FormAddGroup = ({ onAdd }) => {
  const [isDisable, setIsDisable] = useState(false);

  const [faculty, setFaculty] = useState("");
  const [course, setCourse] = useState("");
  const [group, setGroup] = useState("");

  const data = useSelector((state) => {
    return state.data.dataGroups;
  });

  let faculties = data.length
    ? data.map((x) => {
        return { name_faculty: x.name_faculty, _id: x._id };
      })
    : [];

  let courses = faculty.length
    ? data.find((x) => {
        if (x._id == faculty) return true;
      })
    : [];

  let groups = course.length
    ? courses.groups.find((x) => {
        if (x.course == course) return true;
      })
    : [];

  return (
    <View style={styles.container}>
      {faculties.length ? (
        <View style={styles.boxPicker}>
          <Picker
            selectedValue={faculty}
            style={styles.picker}
            onValueChange={(value, idx) => {
              setCourse("");
              setGroup("");
              setFaculty(value);
            }}
          >
            <Picker.Item label="Оберіть факультет!" value="" color="#A0A0A0" />
            {faculties.map((f, idx) => {
              return (
                <Picker.Item
                  key={idx}
                  label={f.name_faculty}
                  value={f._id}
                  color="#000"
                />
              );
            })}
          </Picker>
        </View>
      ) : (
        <AppText style={styles.text}>Факультети відсутні!</AppText>
      )}

      {faculty.length ? (
        <View style={styles.boxPicker}>
          <Picker
            selectedValue={course}
            style={styles.picker}
            onValueChange={(value, idx) => {
              setGroup("");
              setCourse(value);
            }}
          >
            <Picker.Item label="Оберіть курс!" value="" color="#A0A0A0" />
            {courses.groups.map((c, idx) => {
              let course = getCourse(c.course)
              return (
                <Picker.Item
                  key={idx}
                  label={course.name.toString()}
                  value={course.number.toString()}
                  color="#000"
                />
              );
            })}
          </Picker>
        </View>
      ) : null}

      {course.length ? (
        <View style={styles.boxPicker}>
          <Picker
            selectedValue={group}
            style={styles.picker}
            onValueChange={(value, idx) => {
              setGroup(value);
            }}
          >
            <Picker.Item label="Оберіть групу!" value="" color="#A0A0A0" />
            {groups.data.map((g, idx) => {
              return (
                <Picker.Item
                  key={idx}
                  label={g.name_course.toString()}
                  value={g._id.toString()}
                  color="#000"
                />
              );
            })}
          </Picker>
        </View>
      ) : null}

      <View style={styles.boxButton}>
        <Button
          title="Додати групу"
          buttonStyle={{ backgroundColor: THEME.COLOR_INDIGO }}
          disabled={!!!group || isDisable}
          onPress={() => {
            onAdd(group);
            setGroup("");
            setCourse("");
            setFaculty("");
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: "#fff",
  },
  boxPicker: {
    borderBottomColor: "#000",
    borderBottomWidth: 1,
    marginVertical: 5,
  },
  boxButton: { marginTop: 15 },
  picker: {
    height: 50,
    width: Dimensions.get("window").width - 40,
  },
  text: {
    textAlign: "center",
    fontSize: 20,
    color: "#f00",
  },
});
