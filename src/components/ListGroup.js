import React from "react";
import { useSelector } from "react-redux";

import { StyleSheet, View } from "react-native";
import { Icon } from "react-native-elements";
import { AppText } from "./ui";

export const ListGroup = ({ id, onRemove }) => {
  
  const data = useSelector((state) => {
    return state.data.dataGroups;
  });

  const findName = () => {
    let name = "";
    data.forEach((f) => {
      f.groups.forEach((c) => {
        c.data.find((g) => {
          if (id == g._id) {
            name = g.name_course;
          }
        });
      });
    });
    return name;
  };

  let name = findName();

  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <View style={{ flexGrow: 1 }}>
          <AppText style={styles.text}>{name}</AppText>
        </View>
        <View>
          <Icon
            raised
            name="times"
            type="font-awesome"
            color="#f00"
            size={16}
            onPress={() => {
              onRemove(id);
            }}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 5,
    height: 50,
    borderColor: "#000",
    borderWidth: 1,
    borderRadius: 12,
  },
  box: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "stretch",
    alignSelf: "stretch",
  },
  text: {
    fontSize: 24,
    marginHorizontal: 5,
    marginVertical: 10,
  },
});
