import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { CreateQrScreen, GroupsScreen, QrScreen } from "../../screens/qr-code";
import { THEME } from "../../theme";

const Stack = createStackNavigator();

export const QrCodeNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="GroupsScreen"
      screenOptions={{
        headerTintColor: THEME.COLOR_4,
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: THEME.COLOR_1 },
      }}
    >
      <Stack.Screen
        name="GroupsScreen"
        component={GroupsScreen}
        options={{
          title: "Групи:",
        }}
      />
      <Stack.Screen
        name="CreateQrScreen"
        component={CreateQrScreen}
        options={{
          title: "Останні параметри:",
        }}
      />
      <Stack.Screen
        name="QrScreen"
        component={QrScreen}
        options={{
          title: "QR-код",
        }}
      />
    </Stack.Navigator>
  );
};
