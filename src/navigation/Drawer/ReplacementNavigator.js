import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { ReplacementesScreen, GroupsScreen, CreateAndUpdateReplacementScreen } from "../../screens/replacement";
import { THEME } from "../../theme";

const Stack = createStackNavigator();

export const ReplacementNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="ReplacementesScreen"
      screenOptions={{
        headerTintColor: THEME.COLOR_4,
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: THEME.COLOR_1 },
      }}
    >
      <Stack.Screen
        name="ReplacementesScreen"
        component={ReplacementesScreen}
        options={{
          title: "Список замін:",
        }}
      />
       <Stack.Screen
        name="GroupsScreen"
        component={GroupsScreen}
        options={{
          title: "Групи:",
        }}
      />
      <Stack.Screen
        name="CreateAndUpdateReplacementScreen"
        component={CreateAndUpdateReplacementScreen}
        options={{
          title: "Заміна:",
        }}
      />
      
    </Stack.Navigator>
  );
};
