import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  Picker,
  ActivityIndicator,
  Dimensions,
  Text,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { StackActions } from "@react-navigation/native";

import { getDataSubject, postCreatQr } from "../../store/actions";
import { typeOfOccupation as typeLesson } from "../../data";

import { Input, Button } from "react-native-elements";

import { AppText } from "../../components/ui";
import { THEME } from "../../theme";

export const CreateQrScreen = ({ navigation, route }) => {
  const [subject, setSubject] = useState("");
  const [typeOfOccupation, setTypeOfOccupation] = useState("");
  const [audience, setAudience] = useState("");
  const [min, setMin] = useState("");
  const [subgroup, setSubgroup] = useState("all");

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDataSubject());
  }, [dispatch]);

  const subjectes = useSelector((state) => state.data.dataSubject);
  const { loading, disabled } = useSelector((state) => state.data);

  let errorMessageMin = "";
  let validation = false;

  if (!isNaN(min) && min != "") {
    if (+min < 2 || +min > 10) {
      errorMessageMin = "Число повине бути від 2 до 10";
    }
  } else if (!min == "") {
    errorMessageMin = "Введіть будь ласка число!";
  }

  if (
    +min >= 2 &&
    +min <= 10 &&
    !!subject &&
    !!typeOfOccupation &&
    !!audience &&
    route.params?.groups.length
  ) {
    validation = true;
  }

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          {loading ? (
            <ActivityIndicator
              style={{ padding: 40 }}
              size="large"
              color={THEME.COLOR_INDIGO_DARKEN_3}
            />
          ) : (
            <View style={styles.box}>
              {subjectes.length ? (
                <View style={styles.boxPickerAndInput}>
                  <Picker
                    selectedValue={subject}
                    style={styles.picker}
                    onValueChange={(value, idx) => {
                      setSubject(value);
                    }}
                  >
                    <Picker.Item
                      label="Оберіть предмет!"
                      value=""
                      color="#A0A0A0"
                    />
                    {subjectes.map((sub, idx) => {
                      return (
                        <Picker.Item
                          key={idx}
                          label={sub.name}
                          value={sub._id}
                          color="#000"
                        />
                      );
                    })}
                  </Picker>
                </View>
              ) : (
                <AppText style={styles.text}>Предмети відсутні!</AppText>
              )}

              <View style={styles.boxPickerAndInput}>
                <Picker
                  selectedValue={typeOfOccupation}
                  style={styles.picker}
                  onValueChange={(value, idx) => {
                    setTypeOfOccupation(value);
                  }}
                >
                  <Picker.Item
                    label="Оберіть тип заняття!"
                    value=""
                    color="#A0A0A0"
                  />
                  {typeLesson.map((x, idx) => (
                    <Picker.Item
                      label={x.title}
                      value={x.value}
                      color="#000"
                      key={idx}
                    />
                  ))}
                </Picker>
              </View>

              {typeOfOccupation == "practice" ? (
                <View style={styles.boxPickerAndInput}>
                  <Picker
                    selectedValue={subgroup}
                    style={styles.picker}
                    onValueChange={(value, idx) => {
                      setSubgroup(value);
                    }}
                  >
                    <Picker.Item label="Вся група" value="all" color="#000" />
                    <Picker.Item
                      label="1 - підгрупа"
                      value="one"
                      color="#000"
                    />
                    <Picker.Item
                      label="2 - підгрупа"
                      value="two"
                      color="#000"
                    />
                  </Picker>
                </View>
              ) : null}

              <View style={styles.boxPickerAndInput}>
                <Input
                  placeholder="Введіть аудиторію!"
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  containerStyle={styles.inputContainerStyle}
                  value={audience}
                  onChangeText={setAudience}
                />
              </View>
              <View style={styles.boxPickerAndInput}>
                <Input
                  placeholder="Введіть тривалість дії QR-коду"
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  containerStyle={styles.inputContainerStyle}
                  value={min}
                  onChangeText={setMin}
                  errorStyle={{ color: "red" }}
                  errorMessage={errorMessageMin}
                />
              </View>
              <View style={styles.boxButton}>
                <Button
                  title="Створити QR-код"
                  buttonStyle={{ backgroundColor: THEME.COLOR_INDIGO }}
                  loading={disabled}
                  disabled={!validation || disabled}
                  onPress={async () => {
                    let dataSubgroup;

                    switch (subgroup) {
                      case "all":
                        dataSubgroup = [1, 2];
                        break;
                      case "one":
                        dataSubgroup = [1];
                        break;
                      case "two":
                        dataSubgroup = [2];
                        break;
                    }

                    let data = {
                      audience,
                      min,
                      subject,
                      typeOfOccupation,
                      groups: route.params.groups,
                      subgroup: dataSubgroup,
                    };
                    await dispatch(postCreatQr(data));

                    navigation.dispatch(StackActions.replace("QrScreen"));
                  }}
                />
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  box: {
    padding: 10,
    backgroundColor: "#fff",
  },
  boxPickerAndInput: {
    borderBottomColor: "#000",
    borderBottomWidth: 1,
    marginVertical: 5,
  },
  picker: {
    height: 50,
    width: Dimensions.get("window").width - 40,
  },
  inputContainerStyle: { paddingLeft: 0, paddingRight: 0, height: 40 },
  boxButton: { marginTop: 25 },
  text: {
    textAlign: "center",
    fontSize: 20,
    color: "#f00",
  },
});
