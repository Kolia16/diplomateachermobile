import React, { useState, useEffect } from "react";
import { View, StyleSheet, SafeAreaView, ScrollView, ActivityIndicator} from "react-native";
import { StackActions } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "react-native-elements";

import { getDataGroups } from "../../store/actions";

import { THEME } from "../../theme";
import { FormAddGroup, ListGroup } from "../../components";

export const GroupsScreen = ({ navigation }) => {
  const [groups, setGroups] = useState([]);

  const dispatch = useDispatch();

  const { loading } = useSelector((state) => state.data);

  useEffect(() => {
    dispatch(getDataGroups());
  }, [dispatch]);

  const onAddGroup = (group) => {
    if (!groups.find((x) => x === group)) {
      setGroups((prev) => [...prev, group]);
    } else {
      alert("Дана група вже є в списку!");
    }
  };

  const onRemoveGroup = (id) => {
    let newGroups = groups.filter((g) => g !== id);
    setGroups(newGroups);
  };

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          {loading ? (
            <ActivityIndicator
              style={{ padding: 40 }}
              size="large"
              color={THEME.COLOR_INDIGO_DARKEN_3}
            />
          ) : (
            <View>
              <FormAddGroup onAdd={onAddGroup} />
              <View style={styles.boxList}>
                {groups.map((id, idx) => {
                  return (
                    <ListGroup key={idx} id={id} onRemove={onRemoveGroup} />
                  );
                })}
              </View>
              <View style={styles.boxButton}>
                <Button
                  title="Далі"
                  buttonStyle={{ backgroundColor: THEME.COLOR_INDIGO }}
                  disabled={!!!groups.length}
                  onPress={() => {
                    navigation.dispatch(
                      StackActions.push("CreateQrScreen", { groups })
                    );
                  }}
                />
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  boxList: {
    marginTop: 15,
    padding: 10,
    backgroundColor: "white",
  },
  boxButton: {
    marginTop: 15,
    marginBottom: 10,
  },
});
