import React from "react";
import { View, StyleSheet, ActivityIndicator } from "react-native";
import { useSelector } from "react-redux";

import QRCode from "react-native-qrcode-svg";

import { AppText } from "../../components/ui";
import { THEME } from "../../theme";
import { w } from "../../constants";

export const QrScreen = ({ navigation }) => {
  const { loading, qr } = useSelector((state) => state.data);

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator
          style={{ padding: 40 }}
          size="large"
          color={THEME.COLOR_INDIGO_DARKEN_3}
        />
      ) : (
        <View style={styles.box}>
          {qr ? (
            <QRCode value={qr} size={(w / 100) * 90} color={THEME.COLOR_INDIGO}/>
          ) : (
            <AppText style={styles.text}>
              Відсутні дані для відтворення QR-коду!
            </AppText>
          )}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  box: {
    padding: 10,
    backgroundColor: "#fff",
  },
  text: {
    textAlign: "center",
    fontSize: 20,
    color: "#f00",
  },
});
