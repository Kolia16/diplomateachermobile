import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  Picker,
  ActivityIndicator,
  Dimensions,
  Alert,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { StackActions } from "@react-navigation/native";
import { Input, Button } from "react-native-elements";

import { getDataSubject } from "../../store/actions";
import { typeOfOccupation as typeLesson, timeLesson } from "../../data";
import { createReplacement, updateReplacement } from "../../store/actions";

import { AppText } from "../../components/ui";
import { DatePicker } from "../../components";
import { THEME } from "../../theme";

export const CreateAndUpdateReplacementScreen = ({ navigation, route }) => {
  const [subject, setSubject] = useState("");
  const [typeOfOccupation, setTypeOfOccupation] = useState("");
  const [audience, setAudience] = useState("");
  const [numberLessons, setNumberLessons] = useState(null);
  const [numberOfLessons, setNumberOfLessons] = useState(null);
  const [dateCancel, setDateCancel] = useState("");
  const [dateCarryover, setDateCarryover] = useState("");
  const [updateDate, setUpdateDate] = useState("");

  const dispatch = useDispatch();
  let arrNumberOfLessons = [];

  useEffect(() => {
    dispatch(getDataSubject());
  }, [dispatch]);

  const subjectes = useSelector((state) => state.data.dataSubject);
  const { loading } = useSelector((state) => state.data);
  const { disabled } = useSelector((state) => state.replacement);

  useEffect(() => {
    if (route.params.replacement) {
      const p = route.params.replacement.replacement;
      setAudience(p.audience);
      setTypeOfOccupation(p.type_of_occupation);
      setNumberLessons(p.number_lessons);
      setNumberOfLessons(p.number_of_lessons);
      setSubject(p.subject._id);
      setDateCancel(p.date_cancel);
      setDateCarryover(p.date_carryover);
      setUpdateDate(p.date_cancel);
    }
  }, []);

  switch (numberLessons) {
    case 1:
      arrNumberOfLessons = [1, 2, 3, 4, 5, 6];
      break;
    case 2:
      arrNumberOfLessons = [1, 2, 3, 4, 5];
      break;
    case 3:
      arrNumberOfLessons = [1, 2, 3, 4];
      break;
    case 4:
      arrNumberOfLessons = [1, 2, 3];
      break;
    case 5:
      arrNumberOfLessons = [1, 2];
      break;
    case 6:
      arrNumberOfLessons = [1];
      break;
    default:
      arrNumberOfLessons = [1, 2, 3, 4, 5, 6];
      break;
  }

  let validation = false;

  if (
    !!subject &&
    !!typeOfOccupation &&
    !!audience &&
    !!numberLessons &&
    !!numberOfLessons &&
    !!dateCancel &&
    !!dateCarryover &&
    route.params?.groups.length
  ) {
    validation = true;
  }

  const onCallbackDateCancel = (selectedDate) => {
    if (selectedDate) setDateCancel(selectedDate);
  };
  const onCallbackDateCarryover = (selectedDate) => {
    if (selectedDate) setDateCarryover(selectedDate);
  };

  const printError = (error) =>
    Alert.alert("Помилка", `${error.errorMessage}`, [{ text: "OK" }]);

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          {loading ? (
            <ActivityIndicator
              style={{ padding: 40 }}
              size="large"
              color={THEME.COLOR_INDIGO_DARKEN_3}
            />
          ) : (
            <View style={styles.box}>
              {subjectes.length ? (
                <View style={styles.boxPickerAndInput}>
                  <Picker
                    selectedValue={subject}
                    style={styles.picker}
                    onValueChange={(value, idx) => {
                      setSubject(value);
                    }}
                  >
                    <Picker.Item
                      label="Оберіть предмет!"
                      value=""
                      color="#A0A0A0"
                    />
                    {subjectes.map((sub, idx) => {
                      return (
                        <Picker.Item
                          key={idx}
                          label={sub.name}
                          value={sub._id}
                          color="#000"
                        />
                      );
                    })}
                  </Picker>
                </View>
              ) : (
                <AppText style={styles.text}>Предмети відсутні!</AppText>
              )}

              <View style={styles.boxPickerAndInput}>
                <Picker
                  selectedValue={typeOfOccupation}
                  style={styles.picker}
                  onValueChange={(value, idx) => {
                    setTypeOfOccupation(value);
                  }}
                >
                  <Picker.Item
                    label="Оберіть тип заняття!"
                    value=""
                    color="#A0A0A0"
                  />
                  {typeLesson.map((x, idx) => (
                    <Picker.Item
                      label={x.title}
                      value={x.value}
                      color="#000"
                      key={idx}
                    />
                  ))}
                </Picker>
              </View>

              {
                //!!!!!!!!!!!!!!!!!!!!!!!
              }

              <View style={styles.boxPickerAndInput}>
                <Picker
                  selectedValue={numberLessons}
                  style={styles.picker}
                  onValueChange={(value, idx) => {
                    setNumberOfLessons(null);
                    setNumberLessons(value);
                  }}
                >
                  <Picker.Item
                    label="Оберіть час заняття!"
                    value={null}
                    color="#A0A0A0"
                  />
                  {timeLesson.map((x, idx) => (
                    <Picker.Item
                      label={x.time}
                      value={x.number}
                      color="#000"
                      key={idx}
                    />
                  ))}
                </Picker>
              </View>

              <View style={styles.boxPickerAndInput}>
                <Picker
                  selectedValue={numberOfLessons}
                  style={styles.picker}
                  onValueChange={(value, idx) => {
                    setNumberOfLessons(value);
                  }}
                >
                  <Picker.Item
                    label="Оберіть кількість занять!"
                    value={null}
                    color="#A0A0A0"
                  />
                  {arrNumberOfLessons.map((x, idx) => (
                    <Picker.Item
                      label={x.toString()}
                      value={x}
                      color="#000"
                      key={idx}
                    />
                  ))}
                </Picker>
              </View>

              <View style={styles.boxPickerAndInput}>
                <Input
                  placeholder="Введіть аудиторію!"
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  containerStyle={styles.inputContainerStyle}
                  value={audience}
                  onChangeText={setAudience}
                />
              </View>

              <View style={styles.boxDatePicker}>
                <DatePicker
                  startDateDefault={false}
                  onCallbackFunction={onCallbackDateCancel}
                  data={dateCancel}
                  msg="Дата скасованого заняття!"
                  updateDate={updateDate ? updateDate : undefined}
                />
                <DatePicker
                  startDateDefault={true}
                  onCallbackFunction={onCallbackDateCarryover}
                  data={dateCarryover}
                  msg="Дата на яку переноситься заняття!"
                />
              </View>

              <View style={styles.boxButton}>
                <Button
                  title="Створити заміну"
                  buttonStyle={{ backgroundColor: THEME.COLOR_INDIGO }}
                  loading={disabled}
                  disabled={!validation || disabled}
                  onPress={async () => {
                    let data = {
                      audience,
                      subject,
                      typeOfOccupation,
                      groups: route.params.groups,
                      numberLessons,
                      numberOfLessons,
                      dateCancel,
                      dateCarryover,
                    };

                    let error;

                    if (route.params.replacement) {
                      await dispatch(
                        updateReplacement(
                          route.params.replacement.replacement._id,
                          data,
                          (eM, eS) => {
                            error = {
                              errorMessage: eM,
                              errorStatus: eS,
                            };
                          }
                        )
                      );
                    } else {
                      await dispatch(
                        createReplacement(data, (eM, eS) => {
                          error = {
                            errorMessage: eM,
                            errorStatus: eS,
                          };
                        })
                      );
                    }

                    if (!!error) {
                      printError(error);
                    } else {
                      navigation.dispatch(StackActions.popToTop());
                    }
                  }}
                />
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  box: {
    padding: 10,
    backgroundColor: "#fff",
  },
  boxPickerAndInput: {
    borderBottomColor: "#000",
    borderBottomWidth: 1,
    marginVertical: 5,
  },
  picker: {
    height: 50,
    width: Dimensions.get("window").width - 40,
  },
  inputContainerStyle: { paddingLeft: 0, paddingRight: 0, height: 40 },
  boxButton: { marginTop: 25 },
  text: {
    textAlign: "center",
    fontSize: 20,
    color: "#f00",
  },
  boxDatePicker: {
    marginTop: 10,
    flex: 1,
    flexDirection: "row",
  },
});
