import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StackActions } from "@react-navigation/native";
import { StyleSheet, ActivityIndicator, View, FlatList } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Button } from "react-native-elements";

import { CardReplacement } from "../../components";
import { THEME } from "../../theme";
import { AppTextBold } from "../../components/ui";

import { getReplacementes } from "../../store/actions";

export const ReplacementesScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const { replacementes, loading } = useSelector((state) => state.replacement);

  useEffect(() => {
    dispatch(getReplacementes());
  }, [dispatch]);

  const onPushWithCard = (id) => {
    const replacement = replacementes.find((x) => x._id == id);
    navigation.dispatch(
      StackActions.push("GroupsScreen", {
        replacement,
      })
    );
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator
          style={{ padding: 40 }}
          size="large"
          color={THEME.COLOR_INDIGO_DARKEN_3}
        />
      ) : (
        <View style={{ height: "100%" }}>
          <View>
            {replacementes.length ? (
              <FlatList
                keyExtractor={(item) => item._id}
                data={replacementes}
                renderItem={({ item }) => (
                  <CardReplacement
                    replacement={item}
                    onPush={onPushWithCard}
                  ></CardReplacement>
                )}
              />
            ) : (
              <View style={styles.boxMsg}>
                <AppTextBold style={styles.textMsg}>Замін не має!</AppTextBold>
              </View>
            )}
          </View>
          <View
            style={{
              position: "absolute",
              left: 10,
              bottom: 30,
            }}
          >
            <Button
              icon={<Icon name="plus-circle" size={55} color="#f00" />}
              buttonStyle={{
                backgroundColor: "transparent",
              }}
              onPress={() => {
                navigation.dispatch(StackActions.push("GroupsScreen"));
              }}
            />
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  boxMsg: {
    backgroundColor: "#fff",
    margin: 20,
    borderRadius: 24,
    padding: 5,
  },
  textMsg: {
    textAlign: "center",
  },
});
