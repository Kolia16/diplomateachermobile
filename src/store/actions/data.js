import { Http } from "../http";
import {
  GET_DATA_GROUPS,
  GET_DATA_SUBJECT,
  LOADING_AND_DISABLED_DATA,
  POST_CREAT_QR,
} from "../types";

export const getDataGroups = () => {
  return (dispatch) => {
    dispatch({
      type: LOADING_AND_DISABLED_DATA,
      payload: {
        loading: true,
      },
    });

    Http.get(dispatch, "/group/groups-collapsible").then(
      (response) => {
        dispatch({
          type: GET_DATA_GROUPS,
          payload: {
            data: response.data,
          },
        });
      },
      (error) => {
        console.log(error.message);
      }
    );
  };
};

export const getDataSubject = () => {
  return (dispatch) => {
    dispatch({
      type: LOADING_AND_DISABLED_DATA,
      payload: { loading: true },
    });

    Http.get(dispatch, "/teacher/subjectes").then(
      (response) => {
        dispatch({
          type: GET_DATA_SUBJECT,
          payload: {
            subject: response.data,
          },
        });
      },
      (error) => {
        console.log(error.message);
      }
    );
  };
};

export const postCreatQr = (data) => {
  return async (dispatch) => {
    dispatch({
      type: LOADING_AND_DISABLED_DATA,
      payload: {
        loading: false,
        disabled: true,
      },
    });

    await Http.post(dispatch, "/qr-code", data).then(
      (response) => {
        dispatch({
          type: POST_CREAT_QR,
          payload: {
            qr: response.data,
          },
        });
      },
      (error) => {
        console.log(error.message);
      }
    );
  };
};


