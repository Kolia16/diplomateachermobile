import { Http } from "../http";
import {
  GET_REPLACEMENTES,
  LOADING_AND_DISABLED_REPLACEMENT,
  CREATE_REPLACEMENT,
  UPDATE_REPLACEMENT,
  REMOVE_REPLACEMENT,
} from "../types";

export const getReplacementes = () => {
  return (dispatch) => {
    dispatch({
      type: LOADING_AND_DISABLED_REPLACEMENT,
      payload: {
        loading: true,
      },
    });

    Http.get(dispatch, "/replacement").then(
      (replacementes) => {
        dispatch({
          type: GET_REPLACEMENTES,
          payload: {
            replacementes: replacementes.data,
          },
        });
      },
      (error) => {
        console.log(error.message);
      }
    );
  };
};

export const createReplacement = (data, errorFuncCallback) => {
  return async (dispatch) => {
    dispatch({
      type: LOADING_AND_DISABLED_REPLACEMENT,
      payload: {
        disabled: true,
      },
    });

    await Http.post(dispatch, "/replacement", data).then(
      (replacement) => {
        dispatch({
          type: CREATE_REPLACEMENT,
          payload: replacement.data,
        });
      },
      (error) => {
        errorFuncCallback(error.response.data.message, error.response.status);
        dispatch({
          type: LOADING_AND_DISABLED_REPLACEMENT,
          payload: {},
        });
      }
    );
  };
};

export const updateReplacement = (id, data, errorFuncCallback) => {
  return async (dispatch) => {
    dispatch({
      type: LOADING_AND_DISABLED_REPLACEMENT,
      payload: {
        disabled: true,
      },
    });

    await Http.patch(dispatch, `/replacement/${id}`, data).then(
      (replacement) => {
        dispatch({
          type: UPDATE_REPLACEMENT,
          payload: replacement.data,
        });
      },
      (error) => {
        errorFuncCallback(error.response.data.message, error.response.status);
        dispatch({
          type: LOADING_AND_DISABLED_REPLACEMENT,
          payload: {},
        });
      }
    );
  };
};

export const removeReplacement = (id, funcCallback) => {
  return async (dispatch) => {

    await Http.delete(dispatch, `/replacement/${id}`).then(
      (res) => {
        funcCallback(res.data.message)
        dispatch({
          type: REMOVE_REPLACEMENT,
          payload: id,
        });
      },
      (error) => {
        console.log(error.message);
      }
    );
  };
};
