import { createStore, combineReducers, applyMiddleware } from "redux";
// ! для проботи із async action
import thunk from "redux-thunk";
import { authReducer, dataReducer, replacementReducer} from "./reducers";

const rootReducer = combineReducers({
  auth: authReducer,
  data: dataReducer,
  replacement: replacementReducer
});

export default createStore(rootReducer, applyMiddleware(thunk));
