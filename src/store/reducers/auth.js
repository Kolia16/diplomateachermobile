import { LOGIN_AUTH, CLEAN_ERROR, AUTH_DISABLED } from "../types";

const initialState = {
  isAuthorization: true,
  error: null,
  message: "",
  disabled: false,
  loading: false
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_DISABLED:
      return {
        ...state,
        disabled: true
      }

    case LOGIN_AUTH:
      return {
        ...state,
        isAuthorization: action.payload.isAuthorization || false,
        error: action.payload.error || null,
        message: action.payload.message || "",
        disabled: false
      };

    case CLEAN_ERROR:
      return {
        ...state,
        isAuthorization: false,
        error: null,
        message: "",
      };
    default:
      return state;
  }
};
