import {
  GET_DATA_GROUPS,
  GET_DATA_SUBJECT,
  LOADING_AND_DISABLED_DATA,
  POST_CREAT_QR,
} from "../types";

const initialState = {
  dataGroups: [],
  dataSubject: [],
  qr: "",
  loading: false,
  disabled: false,
};

export const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_AND_DISABLED_DATA:
      return {
        ...state,
        loading: action.payload.loading || false,
        disabled: action.payload.disabled || false,
      };

    case GET_DATA_GROUPS:
      return {
        ...state,
        dataGroups: action.payload.data || [],
        loading: false,
      };

    case GET_DATA_SUBJECT:
      return {
        ...state,
        dataSubject: action.payload.subject || [],
        loading: false,
      };

    case POST_CREAT_QR:
      return {
        ...state,
        qr: action.payload.qr || "",
        loading: false,
        disabled: false,
      };


    default:
      return state;
  }
};
