import {
  GET_REPLACEMENTES,
  LOADING_AND_DISABLED_REPLACEMENT,
  CREATE_REPLACEMENT,
  UPDATE_REPLACEMENT,
  REMOVE_REPLACEMENT,
} from "../types";

const initialState = {
  replacementes: [],
  loading: false,
  disabled: false,
};

export const replacementReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_AND_DISABLED_REPLACEMENT:
      return {
        ...state,
        loading: action.payload.loading || false,
        disabled: action.payload.disabled || false,
      };

    case GET_REPLACEMENTES:
      return {
        ...state,
        replacementes: action.payload.replacementes || [],
        loading: false,
      };

    case CREATE_REPLACEMENT:
      return {
        ...state,
        replacementes: [action.payload, ...state.replacementes],
        disabled: false,
      };

    case UPDATE_REPLACEMENT:
      return {
        ...state,
        replacementes: [
          action.payload,
          ...state.replacementes.filter((x) => x._id !== action.payload._id),
        ],
        disabled: false,
      };

    case REMOVE_REPLACEMENT:
      return {
        ...state,
        replacementes: state.replacementes.filter(
          (x) => x._id !== action.payload
        ),
      };

    default:
      return state;
  }
};
